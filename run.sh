#!/bin/bash

uage()
{
    echo "usage: recommendation training and prediction [[[-f | --file python file to excute ] | [-h | --help]]"
}

file=""
port=5007

while [ "$1" != "" ]; do
    case $1 in
        -f | --file )           shift
                                file=$1
                                ;;
        -p | --port )           shift
                                port=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [ "$file" = "" ]; then
	echo "Error: file name must be provided"
    exit
fi

if [[ ! -f $file ]]; then
    echo "$file is not a valid python file"
    exit 1
fi

case $file in
  /*) file=$file ;;
  *) file="${PWD}/${file}" ;;
esac

spark-submit --master yarn --deploy-mode client --executor-memory 1G  --driver-memory 3G \
--name asl-prediction --jars ./spark-streaming-kafka-0-8-assembly_2.11-2.4.3.jar  --conf "spark.app.id=mlib_demo" ${file} -p ${port}
