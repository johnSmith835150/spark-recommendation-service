from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.sql.types import StructType, StructField, StringType

from pyspark.streaming.kafka import KafkaUtils
#from kafka import SimpleProducer, KafkaClient
from kafka import KafkaProducer
from pyspark.sql import Row , HiveContext
from pyspark.sql import SQLContext , DataFrame
from pyspark.ml.recommendation import ALS, ALSModel
from pyspark.sql import Row
#from operator import add
from pyspark.sql import Row
from pyspark.sql.functions import to_timestamp
from pyspark.sql.functions import udf ,col
from collections import OrderedDict
from pyspark.sql.functions import monotonically_increasing_id
import time
import json
import sys
import datetime

loaded_model = None
sc = None
MODEL_PATH = "hdfs:///user/root/als2.model"
NUM_RECOMMENDED_ITEMS = 10
producer = KafkaProducer(bootstrap_servers=['192.168.23.140:9092', '192.168.23.141:9092', '192.168.23.142:9092'], value_serializer=lambda x: json.dumps(x).encode('utf-8'))

def get_model(path):
   global loaded_model
   if loaded_model == None:
      loaded_model = ALSModel.load(path)



def extractProductIds(items):
    return [item[0] for item in items]

def getHiveContextInstance(sparkContext):
   if ('hiveContextSingletonInstance' not in globals()):
       globals()['hiveContextSingletonInstance'] = HiveContext(sparkContext)
   return globals()['hiveContextSingletonInstance']

def getSparkContext():
    global sc 
    if sc == None:
        sc = SparkContext(appName="streamPredicition") 

def recommendProducts(context,id):
    global sc
    specificRecs = loaded_model.recommendForUserSubset(id, NUM_RECOMMENDED_ITEMS)

    extractProductIdsUdf = udf(extractProductIds)
    specificRecs = specificRecs.withColumn("recommendations", extractProductIdsUdf("recommendations"))
    productIdsRows = specificRecs.select('recommendations').collect()
    productIds = []
    for ids in productIdsRows:
        productIds.append(eval(ids[0]))

    
    idsRows = id.collect()
  
    userIds = []
    for row in idsRows:
        userIds.append(int(row[0])) 
         
    for i in range(0,len(productIds)):
        recommend = {
            "userId": userIds[i],
            "timestamp": int(round(time.time())),
            "productIds": productIds[i]
        }

        producer.send('recommendations-list', value=recommend)
        producer.flush()
        now = datetime.datetime.now()
        f = '%Y-%m-%d %H:%M:%S'
        recommend["timestamp"] = now.strftime(f)   
        print(recommend)
        data = sc.parallelize([recommend])
        df = getHiveContextInstance(sc).createDataFrame(data)
        print("ana recommend")
        df.write.mode('append').format("parquet").saveAsTable("recommend")
       


def process(rdd):
    if rdd.isEmpty():
      return
    context = getHiveContextInstance(rdd.context)
    data = rdd.map(lambda w: json.loads(w[1]))    

    df = context.createDataFrame(data ,samplingRatio=0.2)
    df.show(20)
    
    
    login = df.filter(col("event") == "login")
    logout = df.filter(col("event") == "logout")
    Register = df.filter(col("event")== "register")
    createNewProduct = df.filter(col("event") == "createNewProduct")
    viewProduct = df.filter(col("event") =="viewProduct" )
    transaction = df.filter(col("event") == "createOrder")
    
    k = ("event","userId")
    df = df.select(*k)
    df.write.mode('append').format("parquet").saveAsTable("events")
   

    if login.count() > 0 :
        recommendProducts(context,login.select('userId'))
        print("ana login")
        login.show()
        l = ("timestamp","userId")
        login = login.select(*l)
        login = login.withColumn('time', col('timestamp').cast('timestamp'))
        login = login.drop("timestamp")
        login.write.mode('append').format("parquet").saveAsTable("login")
   
    if logout.count() > 0:
        print("ana logout")
        l = ("timestamp","userId")
        logout.show()
        logout = logout.select(*l)
        logout.show()
        logout = logout.withColumn('time', col('timestamp').cast('timestamp'))
        logout = logout.drop("timestamp")
        logout.show()
        logout.write.mode('append').format("parquet").saveAsTable("logout")
   
    if Register.count() > 0:
        print("ana register")
        Register.show()
        Register = Register.select("user")
        df = Register.select('user').rdd.map(lambda r: r.user).toDF()
        df11 = df.select("*").withColumn("columnindex", monotonically_increasing_id())
        df22 = Register.select("*").withColumn("columnindex", monotonically_increasing_id())
        Register =  df11.join(df22, df11.columnindex == df22.columnindex, 'inner').drop(df22.columnindex)
        Register = Register.drop("columnindex","user")
        Register = Register.withColumnRenamed("nama", "name")
        Register.printSchema()
        Register.show()
        Register.write.mode('append').format("parquet").saveAsTable("users")
   
    if viewProduct.count() > 0:
        print("ana view")
        l = ("userId","productId","timestamp")
        viewProduct.show()
        viewProduct = viewProduct.select(*l)
        viewProduct = viewProduct.withColumn('time', col('timestamp').cast('timestamp'))
        viewProduct = viewProduct.drop("timestamp")
        viewProduct.write.mode('append').format("parquet").saveAsTable("view")
   
    if transaction.count() > 0:
        print("ana transaction")
        l = ("userId","timestamp","productId")
        transaction.show()
        transaction = transaction.select(*l)
        transaction.show()
        transaction = transaction.withColumn('time', col('timestamp').cast('timestamp'))
        transaction = transaction.drop("timestamp")
        transaction.write.mode('append').format("parquet").saveAsTable("transaction")
   
    if createNewProduct.count() > 0:
        print("ana newproduct")
        l = ("timestamp","product")
        createNewProduct.show()
        createNewProduct = createNewProduct.select(*l)
        df = createNewProduct.select('product').rdd.map(lambda r: r.product).toDF()
        df11 = df.select("*").withColumn("columnindex", monotonically_increasing_id())
        df22 = createNewProduct.select("*").withColumn("columnindex", monotonically_increasing_id())
        createNewProduct =  df11.join(df22, df11.columnindex == df22.columnindex, 'inner').drop(df22.columnindex)
        createNewProduct = createNewProduct.drop("columnindex","product")
        #createNewProduct = createNewProduct.withColumn('time', col('timestamp').cast('timestamp'))
        
        createNewProduct = createNewProduct.drop("timestamp","userId")
        createNewProduct = createNewProduct.withColumnRenamed("nama", "name")
        createNewProduct = createNewProduct.withColumnRenamed("harga", "price")
        createNewProduct.write.mode('append').format("parquet").saveAsTable("products")



if __name__ == "__main__":
   
   topic = "events-log"
   brokers = "192.168.23.140:9092,192.168.23.141:9092,192.168.23.142:9092"
   getSparkContext()
   ssc = StreamingContext(sc, 1)
   sqlContext = SQLContext(sc)
   get_model(MODEL_PATH)
   kafkaParams = {"metadata.broker.list": brokers, "auto.offset.reset": "smallest"}
   kafkaDStream = KafkaUtils.createDirectStream(ssc, [topic], kafkaParams)
   kafkaDStream.foreachRDD(process)


   ssc.start()
   ssc.awaitTermination()
