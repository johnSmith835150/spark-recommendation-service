




from pyspark.sql.functions import col

from pyspark.sql import SparkSession

from pyspark.sql.types import *

spark = SparkSession.builder.master("yarn").appName('alsModel').getOrCreate()
users = spark.read.load('users.csv', format ="csv" ,header='true', inferschema='true')
products = spark.read.load('products.csv', format ="csv" ,header='true', inferschema='true')


users = users.withColumn('id', col('id').cast('string'))
products = products.withColumn('id', col('id').cast('string'))
products = products.withColumn('rate', col('rate').cast('string'))
products = products.withColumn('stock', col('stock').cast('string'))

users.show(5)
products.show(5)

products.write.mode('append').format("parquet").saveAsTable("products")
users.write.mode('append').format("parquet").saveAsTable("users")
